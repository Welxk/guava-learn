package com.cache;

import com.google.common.base.Optional;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

/**
 * Created by Lxk on 2019/10/13.
 */
public class CacheLoaderLearn3 {

    /**
     * cache从缓存中获取不能为空，为空报错
     */
    public void testLoadNullValue(){
        CacheLoader<String,Employee> cacheLoader = CacheLoader
                .from(k -> k.equals("null") ? null : new Employee(k,k + " level"));
        LoadingCache<String,Employee> loadingCache = CacheBuilder.newBuilder().build(cacheLoader);
        Employee alex = loadingCache.getUnchecked("Alex");

        Employee nullValue = loadingCache.getUnchecked("null");
    }

    public void testOptionNullValue(){
        CacheLoader<String,Optional<Employee>> loader = new CacheLoader<String, Optional<Employee>>() {
            @Override
            public Optional<Employee> load(String s) throws Exception {
                if(s.equals("null")){
                    return Optional.fromNullable(null);
                }else{
                    return Optional.fromNullable(new Employee(s,s + "level"));
                }
            }
        };
        LoadingCache<String,Optional<Employee>> cache = CacheBuilder.newBuilder()
                                                                    .build(loader);

        System.out.println(cache.getUnchecked("Alex").get());
        System.out.println(cache.getUnchecked("null").orNull());
    }

    public static void main(String[] args) {
        CacheLoaderLearn3 cacheLoaderLearn = new CacheLoaderLearn3();
        cacheLoaderLearn.testOptionNullValue();
    }

}
