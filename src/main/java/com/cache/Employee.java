package com.cache;

/**
 * Created by Lxk on 2019/10/13.
 */
public class Employee {

    private String name;

    private String level;

    public Employee(String name, String level) {
        this.name = name;
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public String getLevel() {
        return level;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", level='" + level + '\'' +
                '}';
    }
}
