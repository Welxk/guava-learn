package com.cache;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * Created by Lxk on 2019/10/13.
 */
public class CacheLoaderLearn {

    private Employee findEmployeeByName(final String name){
        System.out.println("load " + name + "'s info from db!!");
        Random random = new Random();
        return new Employee(name,String.valueOf(random.nextInt(10))+" level");
    }

    public void CacheBasic() throws ExecutionException {
        LoadingCache<String,Employee> cache = CacheBuilder.newBuilder().maximumSize(10)
                                                            .expireAfterAccess(10, TimeUnit.SECONDS)
                                                            .build(new CacheLoader<String, Employee>() {
                                                                @Override
                                                                public Employee load(String name) throws Exception {
                                                                    return findEmployeeByName(name);
                                                                }
                                                            });
        Employee employee = cache.get("Lee");
        System.out.println(employee);
        Employee employee2 = cache.get("Lee");
        System.out.println(employee2);
    }

    public static void main(String[] args) throws ExecutionException {
        CacheLoaderLearn learn = new CacheLoaderLearn();
        learn.CacheBasic();
    }

}
