package com.Joiner;

import com.google.common.base.Joiner;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Lxk on 2019/7/9.
 */
public class JoinerLearn {

    public static void main(String[] args) {
        List<String> arr = Arrays.asList("ctrip","google","jd",null);
        String res = Joiner.on("@").useForNull("null").join(arr);
        System.out.println(res);
    }

}
