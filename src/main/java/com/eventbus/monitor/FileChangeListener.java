package com.eventbus.monitor;

import com.google.common.eventbus.Subscribe;

/**
 * Created by Lxk on 2020/7/25.
 */
public class FileChangeListener {

    @Subscribe
    public void onChange(FileChangeEvent fileChangeEvent){
        System.out.println(fileChangeEvent.toString());
    }

}
