package com.eventbus.monitor;

import com.eventbus.listener.ExceptionListener;

/**
 * Created by Lxk on 2020/7/25.
 */
public interface TargetMonitor {

    void startMonitor() throws Exception;

    void stopMonitor() throws Exception;

}
