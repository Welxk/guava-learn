package com.eventbus.monitor;

import com.google.common.eventbus.EventBus;

/**
 * Created by Lxk on 2020/7/25.
 */
public class Main {

    public static void main(String[] args) throws Exception {
        EventBus eventBus = new EventBus();
        eventBus.register(new FileChangeListener());
        TargetMonitor monitor = new DirTargetMonitor(eventBus,"C:\\Users\\Lxk\\Pictures\\Camera Roll");
        monitor.startMonitor();
    }

}
