package com.eventbus;

import com.eventbus.listener.ExceptionListener;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.SubscriberExceptionContext;
import com.google.common.eventbus.SubscriberExceptionHandler;

/**
 * Created by Lxk on 2020/7/24.
 */
public class ExceptionExample {

    /**
     * 异常并不会阻塞下一个handler的派发
     * @param args
     */
    public static void main(String[] args) {
        final EventBus eventBus = new EventBus(new ExceptionHandler());
        eventBus.register(new ExceptionListener());
        eventBus.post("event");
    }

    static class ExceptionHandler implements SubscriberExceptionHandler {

        @Override
        public void handleException(Throwable throwable, SubscriberExceptionContext subscriberExceptionContext) {
            System.out.println(subscriberExceptionContext.getEvent());
            System.out.println(subscriberExceptionContext.getEventBus());
            System.out.println(subscriberExceptionContext.getSubscriber());
            System.out.println(subscriberExceptionContext.getSubscriberMethod());
        }
    }

}
