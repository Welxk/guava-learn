package com.eventbus;

import com.eventbus.listener.AbstractListener;
import com.eventbus.listener.ConcreteListener;
import com.google.common.eventbus.EventBus;

/**
 * Created by Lxk on 2020/7/24.
 */
public class InheritListenerExample {

    /**
     * 测试监听者继承情况下的事件派发机制
     * @param args
     */
    public static void main(String[] args) {
        final EventBus eventBus = new EventBus();
        AbstractListener listener = new ConcreteListener();
        eventBus.register(listener);
        eventBus.post("hello");
        eventBus.unregister(listener);
        eventBus.post("hello");
    }

}
