package com.eventbus.listener;

import com.google.common.eventbus.Subscribe;

/**
 * Created by Lxk on 2020/7/24.
 */
public class ExceptionListener {

    @Subscribe
    public void doAction(String msg){
        System.out.println("doAction String: " + msg);
    }

    @Subscribe
    public void doAction2(String msg){
        System.out.println("doAction2 String: " + msg);
        throw new RuntimeException("exception");
    }

    @Subscribe
    public void doAction3(String msg){
        System.out.println("doAction3 String: " + msg);
    }


}
