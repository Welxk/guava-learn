package com.eventbus.listener;

import com.google.common.eventbus.Subscribe;

import java.util.logging.Logger;

/**
 * Created by Lxk on 2019/7/13.
 */
public class SimpleListener {

    @Subscribe
    public void doAction(final String event){
        System.out.println(String.format("get event:%s,do Action",event));
    }

}
