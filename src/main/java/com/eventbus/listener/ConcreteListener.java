package com.eventbus.listener;

import com.google.common.eventbus.Subscribe;

/**
 * Created by Lxk on 2020/7/24.
 */
public class ConcreteListener extends BaseListener {

    @Subscribe
    public void conTask(String msg){
        System.out.println("conTask String: " + msg);
    }


}
