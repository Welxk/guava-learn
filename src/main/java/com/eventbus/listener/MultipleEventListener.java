package com.eventbus.listener;

import com.google.common.eventbus.Subscribe;

/**
 * Created by Lxk on 2020/7/24.
 */
public class MultipleEventListener {

    @Subscribe
    public void doAction(String msg){
        System.out.println("doAction String: " + msg);
    }

    @Subscribe
    public void doAction2(String msg){
        System.out.println("doAction2 String: " + msg);
    }

    @Subscribe
    public void doAction3(Integer msg){
        System.out.println("doAction3 Integer: " + msg);
    }

}
