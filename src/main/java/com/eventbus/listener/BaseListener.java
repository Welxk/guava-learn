package com.eventbus.listener;

import com.google.common.eventbus.Subscribe;

/**
 * Created by Lxk on 2020/7/24.
 */
public class BaseListener extends AbstractListener{

    @Subscribe
    public void baseTask(String msg){
        System.out.println("baseTask String: " + msg);
    }

}
