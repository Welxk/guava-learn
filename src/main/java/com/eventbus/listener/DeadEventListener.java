package com.eventbus.listener;

import com.google.common.eventbus.DeadEvent;
import com.google.common.eventbus.Subscribe;

/**
 * Created by Lxk on 2020/7/24.
 */
public class DeadEventListener {

    @Subscribe
    public void doHandle(DeadEvent event){
        System.out.println(event.getSource());
        System.out.println(event.getEvent());
    }

}
