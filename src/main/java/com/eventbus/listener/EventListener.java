package com.eventbus.listener;

import com.eventbus.event.BaseEvent;
import com.eventbus.event.ConcreteEvent;
import com.google.common.eventbus.Subscribe;

/**
 * Created by Lxk on 2020/7/24.
 */
public class EventListener {

    @Subscribe
    public void doAction(BaseEvent event){
        System.out.println("doAction BaseEvent: " + event);
    }

    @Subscribe
    public void doAction2(ConcreteEvent event){
        System.out.println("doAction2 ConcreteEvent: " + event);
    }

}
