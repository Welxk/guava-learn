package com.eventbus.listener;

import com.google.common.eventbus.Subscribe;

/**
 * Created by Lxk on 2020/7/24.
 */
public class AbstractListener {

    @Subscribe
    public void commonTask(String msg){
        System.out.println("commonTask String: " + msg);
    }

}
