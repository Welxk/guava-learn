package com.eventbus;

import com.eventbus.event.BaseEvent;
import com.eventbus.event.ConcreteEvent;
import com.eventbus.listener.EventListener;
import com.google.common.eventbus.EventBus;

/**
 * Created by Lxk on 2020/7/24.
 */
public class EventInheritExample {

    /**
     * 测试事件继承结构情况下的派发机制
     * @param args
     */
    public static void main(String[] args) {
        final EventBus eventBus = new EventBus();
        eventBus.register(new EventListener());
        eventBus.post(new BaseEvent("Base Event"));
        eventBus.post(new BaseEvent("Base Event2"));
        eventBus.post(new ConcreteEvent("concreteEvent"));
    }

}
