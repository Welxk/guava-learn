package com.eventbus.event;

/**
 * Created by Lxk on 2020/7/24.
 */
public class ConcreteEvent extends BaseEvent {

    public ConcreteEvent() {
    }

    public ConcreteEvent(String eventMsg) {
        super(eventMsg);
    }
}
