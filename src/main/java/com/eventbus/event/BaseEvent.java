package com.eventbus.event;

/**
 * Created by Lxk on 2020/7/24.
 */
public class BaseEvent {

    private String eventMsg;

    public BaseEvent(){
    }

    public BaseEvent(String eventMsg) {
        this.eventMsg = eventMsg;
    }

    public String getEventMsg() {
        return eventMsg;
    }

    public void setEventMsg(String eventMsg) {
        this.eventMsg = eventMsg;
    }

    @Override
    public String toString() {
        return "BaseEvent{" +
                "eventMsg='" + eventMsg + '\'' +
                '}';
    }
}
