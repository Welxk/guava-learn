package com.eventbus;

import com.eventbus.listener.SimpleListener;
import com.google.common.eventbus.EventBus;

/**
 * Created by Lxk on 2019/7/13.
 */
public class SimpleEventBusExample {

    /**
     * 测试简单事件派发机制
     * @param args
     */
    public static void main(String[] args) {
        final EventBus eventBus = new EventBus();
        eventBus.register(new SimpleListener());
        System.out.println("post event:");
        eventBus.post("simple event");
    }
}
