package com.eventbus;

import com.eventbus.listener.MultipleEventListener;
import com.google.common.eventbus.EventBus;

/**
 * Created by Lxk on 2020/7/24.
 */
public class MultipleEventBusExample {

    /**
     * 测试多监听者情况下事件派发机制
     * @param args
     */
    public static void main(String[] args) {
        final EventBus eventBus = new EventBus();
        eventBus.register(new MultipleEventListener());
        eventBus.post("event ");
        eventBus.post("event2 ");
        eventBus.post(1);
        eventBus.post(2);
    }
}
