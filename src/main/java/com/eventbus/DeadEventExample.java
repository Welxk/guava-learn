package com.eventbus;

import com.eventbus.listener.DeadEventListener;
import com.google.common.eventbus.EventBus;

/**
 * Created by Lxk on 2020/7/24.
 */
public class DeadEventExample {

    public static void main(String[] args) {
        final EventBus eventBus = new EventBus("deadEventBus"){
            @Override
            public String toString() {
                return "event-bus";
            }
        };
        eventBus.register(new DeadEventListener());
        eventBus.post("test");

    }

}
