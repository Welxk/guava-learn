package com.concurrent;

import com.google.common.util.concurrent.*;

import java.util.concurrent.*;

/**
 * Created by Lxk on 2019/11/16.
 */
public class ListenerFutureExample {

    /**
     * 该方式需要主动去get值，并且会阻塞future
     */
    public static void testFuture(){
        ExecutorService service = Executors.newFixedThreadPool(2);
        Future<Integer> future = service.submit(()->{
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return 10;
        });
        try {
            Object res = future.get();
            System.out.println(res);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    /**
     * 异步回调方式，被动监听通知，比较优雅,但是回调无法传递入参（处理结果）
     */
    public static void asyncCallbackFuture(){
        ExecutorService service = Executors.newFixedThreadPool(2);
        ListeningExecutorService listeningExecutorService = MoreExecutors.listeningDecorator(service);
        ListenableFuture<Integer> future = listeningExecutorService.submit(()->{
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return 10;
        });
        future.addListener(()-> System.out.println("I am finished"),listeningExecutorService);
        System.out.println("main finish");
    }

    public static void asyncGuava(){
        ExecutorService service = Executors.newFixedThreadPool(2);
        ListeningExecutorService listeningExecutorService = MoreExecutors.listeningDecorator(service);
        ListenableFuture<Integer> future = listeningExecutorService.submit(()->{
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return 10;
        });
        Futures.addCallback(future,new MyCallBack(),listeningExecutorService);
        System.out.println("main finished!!");
    }

    /**
     * 8版本提供的异步回到
     */
    public static void main(String[] args) {
        ExecutorService service = Executors.newFixedThreadPool(2);
        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(()->{
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return 10;
        },service).whenComplete((v,t)-> System.out.println("finished result is : " + v));
        System.out.println("main finished!!");
    }

    static class MyCallBack implements FutureCallback<Integer> {

        @Override
        public void onSuccess(Integer integer) {
            System.out.println("result is :" + integer);
        }

        @Override
        public void onFailure(Throwable throwable) {
            System.out.println("error occur !!");
        }
    }

}
